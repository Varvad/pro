package com.gmail.v.varvaruk89;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class EditorDB {

    public static void addPersonToBD(String lastName, String firstName, String phone, String email) {
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("INSERT INTO person (last_name,first_name,phone,email) VALUES (?,?,?,?)");
            preparedStatement.setString(1, lastName);
            preparedStatement.setString(2, firstName);
            preparedStatement.setString(3, phone);
            preparedStatement.setString(4, email);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void addProductToBD(String name, double price, int quantity) {
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("INSERT INTO product (name,price,quantity) VALUES (?,?,?)");
            preparedStatement.setString(1, name);
            preparedStatement.setDouble(2, price);
            preparedStatement.setInt(3, quantity);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void outInfoAllPerson() {
        Statement statement = Connector.createStatement();
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM person");
            while (rs.next()) {
                Person tmp = new Person();
                tmp.setId(rs.getInt("id"));
                tmp.setLastName(rs.getString("last_name"));
                tmp.setFirstName(rs.getString("first_name"));
                tmp.setPhone(rs.getString("phone"));
                tmp.setEmail(rs.getString("email"));
                System.out.println(tmp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void outInfoAllProducts() {
        Statement statement = Connector.createStatement();
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM product");
            while (rs.next()) {
                Product tmp = new Product();
                tmp.setId(rs.getInt("id"));
                tmp.setName(rs.getString("name"));
                tmp.setPrice(rs.getDouble("price"));
                tmp.setQuantity(rs.getInt("quantity"));
                System.out.println(tmp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkTheQuantity(int quantity, int id) {
        boolean checkTheQuantity = false;
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("SELECT * FROM product WHERE id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            int quantityInDB = 0;
            while (rs.next()) {
                quantityInDB = rs.getInt("quantity");
            }
            if (quantity <= quantityInDB) {
                checkTheQuantity = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return checkTheQuantity;
    }

    public static double priceProduct(int id) {
        double priceProduct = 0;
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("SELECT * FROM product WHERE id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                priceProduct = rs.getDouble("price");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return priceProduct;
    }

    public static Person personFromBD(int idPerson) {
        Person person = new Person();
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("SELECT * FROM person WHERE id = ?");
            preparedStatement.setInt(1, idPerson);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                person.setId(rs.getInt("id"));
                person.setLastName(rs.getString("last_name"));
                person.setFirstName(rs.getString("first_name"));
                person.setFirstName(rs.getString("phone"));
                person.setEmail(rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return person;
    }

    public static Product productFromBD(int idProduct) {
        Product product = new Product();
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("SELECT * FROM product WHERE id = ?");
            preparedStatement.setInt(1, idProduct);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                product.setId(rs.getInt("id"));
                product.setName(rs.getString("name"));
                product.setPrice(rs.getDouble("price"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return product;
    }

    public static void addOrderToBD(Order orderOb) {
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("INSERT INTO orders" +
                    " (person_id,product_id,quantity,sum) VALUES (?,?,?,?)");
            preparedStatement.setInt(1, orderOb.getPersonId());
            preparedStatement.setInt(2, orderOb.getProductId());
            preparedStatement.setInt(3, orderOb.getQuantity());
            preparedStatement.setDouble(4, orderOb.getSum());
            preparedStatement.executeUpdate();
            updateProductQuantity(orderOb);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void updateProductQuantity(Order order) {
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("UPDATE    product SET  quantity = quantity-?  WHERE id =?");
            preparedStatement.setInt(1, order.getQuantity());
            preparedStatement.setInt(2, order.getProductId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
