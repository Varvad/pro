package com.gmail.v.varvaruk89;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;

public class Connector {

    private static final String URL = "jdbc:mysql://localhost:3306/orderdb";
    private static final String USERNAME = "test";
    private static final String PASSWORD = "test";





    public static void creatingTables() {
        Connection connection = createConnection();
        createTablePerson(connection);
        createTableProduct(connection);
        createTableOrders(connection);


    }
    public static Connection createConnection() {
        Connection connection = null;

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static void createTablePerson(Connection connection) {
        try {
            Statement st = connection.createStatement();
            st.execute("DROP TABLE IF EXISTS person");
            st.execute("CREATE  TABLE  person (id INT NOT NULL  AUTO_INCREMENT PRIMARY KEY," +
                    "last_name VARCHAR (128)DEFAULT NULL,first_name VARCHAR (128)DEFAULT NULL," +
                    "phone VARCHAR (12) DEFAULT NULL,email VARCHAR (128)DEFAULT NULL)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void createTableProduct(Connection connection) {
        try {
            Statement st = connection.createStatement();
            st.execute("DROP TABLE IF EXISTS product");
            st.execute("CREATE  TABLE  product (id INT NOT NULL  AUTO_INCREMENT PRIMARY KEY," +
                    "name VARCHAR (128)DEFAULT NULL,price DOUBLE DEFAULT NULL,quantity INT DEFAULT NULL)");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static void createTableOrders(Connection connection) {
        try {
            Statement st = connection.createStatement();
            st.execute("DROP TABLE   IF EXISTS orders");
            st.execute("CREATE  TABLE  orders(id INT NOT NULL  AUTO_INCREMENT PRIMARY KEY,person_id INT NOT NULL,product_id INT NOT NULL ,quantity INT NOT NULL,sum DOUBLE  NOT  NULL ,FOREIGN KEY (person_id) REFERENCES person(id) ,FOREIGN KEY (product_id) REFERENCES  product (id))");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Statement createStatement() {
        Statement statement = null;

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return statement;
    }

}
