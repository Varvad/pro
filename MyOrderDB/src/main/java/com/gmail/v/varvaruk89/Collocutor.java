package com.gmail.v.varvaruk89;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Collocutor {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void communicate() {
        int selection = 0;

        while (selection != 9) {
            System.out.println("Make a choice:");
            System.out.println("Add: 1 - person; 2 - product; 3 - Order.");
            System.out.println("Exit - 9.");

            try {
                selection = Integer.parseInt(reader.readLine());
                if (selection == 1 | selection == 2 | selection == 3 | selection == 9) {
                    if (selection == 1) {
                        inputPerson();
                    }
                    if (selection == 2) {
                        inputProduct();
                    }
                    if (selection == 3) {
                        inputOrder();
                    }
                } else {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException e) {
                System.out.println("press 1 / press 2/press 3. And press Enter!");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private static void inputPerson() {
        try {
            System.out.println("Enter the person's last name:");
            String lastName = reader.readLine();
            System.out.println(lastName);
            System.out.println("Enter the person's name:");
            String name = reader.readLine();
            System.out.println("Enter the phone number of the person:");
            String phone = reader.readLine();
            System.out.println("Enter the person's email address:");
            String email = reader.readLine();
            if (!(lastName.equals("/n")) | name != null | phone != null | email != null) {
                EditorDB.addPersonToBD(lastName, name, phone, email);
                System.out.println("Persons added to the database.");
            } else {
                System.out.println("Person not added, you need to fill in all fields!");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void inputProduct() {
        try {
            System.out.println("Enter the name of the product:");
            String name = reader.readLine();
            System.out.println("Enter price:");
            String price = reader.readLine();
            System.out.println("Enter the quantity:");
            String quantity = reader.readLine();

            if ((name != null) | (price != null) | (quantity != null)) {
                EditorDB.addProductToBD(name, Double.parseDouble(price), Integer.parseInt(quantity));
                System.out.println("product added to the database.");
            } else {
                System.out.println("product not added, you need to fill in all fields!");
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void inputOrder() {
        Order tmp = new Order();
        EditorDB.outInfoAllPerson();
        System.out.println("Enter the client's id:");
        tmp.setPersonId(inputID());
        EditorDB.outInfoAllProducts();
        System.out.println("Enter the product id:");
        tmp.setProductId(inputID());
        System.out.println("Enter the quantity of product:");
        int quantity = inputID();
        if (EditorDB.checkTheQuantity(quantity, tmp.getProductId())) {
            tmp.setQuantity(quantity);
        } else {
            System.out.println("No quantity is available. Select less");
            communicate();
        }
        tmp.setSum(EditorDB.priceProduct(tmp.getProductId()) * quantity);
        orderVerification(tmp);
    }

    private static int inputID() {

        int id = 0;

        try {
            String idString = reader.readLine();
            if (idString == null) {
                throw new IOException();
            } else {
                try {
                    int idInt = Integer.parseInt(idString);
                    id = idInt;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return id;
    }

    private static void orderVerification(Order order) {
        System.out.println("Order verification:");
        System.out.println(EditorDB.personFromBD(order.getPersonId()));
        Product product = EditorDB.productFromBD(order.getProductId());
        product.setQuantity(order.getQuantity());
        System.out.println(product);
        System.out.println("Order for an amount: "+order.getSum());
        System.out.println("1 - Confirm. "+"2 - Change your mind.");
        try {
            String verification = reader.readLine();
            if(verification.equals("2")){
                inputOrder();
            }else if(verification.equals("1")){
                savingOrder(order);
            }else{
                System.out.println("Wrong parameters!");
                communicate();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static  void savingOrder(Order order){
       EditorDB.addOrderToBD(order);

    }


}


