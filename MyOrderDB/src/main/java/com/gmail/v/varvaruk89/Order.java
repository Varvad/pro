package com.gmail.v.varvaruk89;

public class Order {
private int id;
private int personId;
private int productId;
private int quantity;
private double sum;

    public Order(int id, int personId, int productId, int quantity, int sum) {
        this.id = id;
        this.personId = personId;
        this.productId = productId;
        this.quantity = quantity;
        this.sum = sum;
    }

    public Order() {
    }


    public int getId() {
        return id;
    }

    public int getPersonId() {
        return personId;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", personId=" + personId +
                ", productId=" + productId +
                ", quantity=" + quantity +
                '}';
    }
}
