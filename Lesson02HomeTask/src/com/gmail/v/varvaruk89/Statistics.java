package com.gmail.v.varvaruk89;

public class Statistics {
    private int bmw = 0;
    private int chevrolet = 0;
    private int audi = 0;
    private int everything = 0;
    private int sea = 0;
    private int study = 0;
    private int job = 0;
    private int everythingSummer = 0;

    public Statistics() {
    }

    public int getBmw() {
        return bmw;
    }

    public int getChevrolet() {
        return chevrolet;
    }

    public int getAudi() {
        return audi;
    }

    public int getEverything() {
        return everything;
    }

    public int getSea() {
        return sea;
    }

    public int getStudy() {
        return study;
    }

    public int getJob() {
        return job;
    }

    public int getEverythingSummer() {
        return everythingSummer;
    }


    public void carCount (String aCar){
        if(aCar!=null){
            if(aCar.equals("BMW")){
                bmw++;
            }else if (aCar.equals("Chevrolet")){
                chevrolet++;
            }else if (aCar.equals("Audi")){
                audi++;
            }else if (aCar.equals("I choose everything")){
                everything++;
            }
        }
    }

    public void summerCount (String plansSummer){
        if(plansSummer!=null){
            if(plansSummer.equals("Go to the sea")){
                sea++;
            }else if (plansSummer.equals("Study")){
                study++;
            }else if (plansSummer.equals("Looking for a job")){
                job++;
            }else if (plansSummer.equals("I choose everything in the summer")){
                everythingSummer++;
            }
        }
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "bmw=" + bmw +
                ", chevrolet=" + chevrolet +
                ", audi=" + audi +
                ", everything=" + everything +
                ", sea=" + sea +
                ", study=" + study +
                ", job=" + job +
                ", everythingSummer=" + everythingSummer +
                '}';
    }
}
