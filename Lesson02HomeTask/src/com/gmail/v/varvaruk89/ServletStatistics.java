package com.gmail.v.varvaruk89;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@WebServlet(name = "ServletStatistics", urlPatterns = "/anketa")
public class ServletStatistics extends HttpServlet {
    private String myLink = "<a href = \"/welcome.html\">Home</a>";
    private int count = 1;
    private Map<Integer, Anketa> hashMap = new HashMap<>();
    private Statistics stata = new Statistics();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            if ((request.getParameter("fname") == null) | (request.getParameter("sname") == null) | (request.getParameter("age")) == null | (request.getParameter("answerA") == null) | (request.getParameter("answerB") == null)) {
                throw new NumberFormatException();

            }
            {

            }
            Anketa anketa = new Anketa(request.getParameter("fname"), request.getParameter("sname"), Integer.parseInt(request.getParameter("age")), request.getParameter("answerA"), request.getParameter("answerB"));
            hashMap.put(count, anketa);
        } catch (NumberFormatException e) {
            response.getWriter().print("<html>" + "SHUFI . GDE PARAMETRU? Zapolni vse polya. " + "  " + myLink + "</html>");
            return;
        }
        stata.carCount(request.getParameter("answerA"));
        stata.summerCount(request.getParameter("answerB"));
        System.err.println(stata);
        String fname = request.getParameter("fname");
        response.getWriter().print("<html>" + "<head>"
                + "</head>"
                + "<body>"
                + "<fieldset>"
                + "<legend>"
                + "General information:"
                + "</legend>"
                + "<table>" + hashMapToHtml(count)
                + "</table>"
                + "</fieldset>"
                + "<br>"
                + "<fieldset>"
                + "<legend>"
                + "Statistics for the selected brand of car:"
                + "</legend>"
                + "<table>"
                + stataToHtml(count)
                + "</table>"
                + "</fieldset>"
                + "<br>"
                + "<fieldset>"
                + "<legend>"
                + "Statistics plans for the summer:"
                + "</legend>"
                + "<table>"
                + stataToHtmlSummer(count)
                + "</table>"
                + "</fieldset>"
                + "</body>"

                + myLink + "</html>");

        count++;

    }

    public String hashMapToHtml(int count) {
        String toHtml = "<tr align =\"center\">"
                + "<td bgcolor = \"#F1F1F1\">" + "First name" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Last name" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Age" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "A car" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Plans for the summer" + "</td>"
                + "</tr>";
        for (int i = 1; i <= count; i++) {
            Anketa tmp = hashMap.get(i);
            toHtml = toHtml + "<tr align =\"center\">"
                    + "<td bgcolor = \"#F1F1F1\">" + tmp.getFname() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + tmp.getSname() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + tmp.getAge() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + tmp.getaCar() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + tmp.getPlansSummer() + "</td>"
                    + "</tr>";
        }
        return toHtml;
    }

    public String stataToHtml(int count) {
        String stataTohtml = "<tr align =\"center\">"
                + "<td bgcolor = \"#F1F1F1\">" + "BMW" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Chevrolet" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Audi" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "I choose everything" + "</td>"
                + "</tr>";
        for (int i = 1; i <= count; i++) {
            Anketa tmp = hashMap.get(i);
            stataTohtml = stataTohtml + "<tr align =\"center\">"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getBmw() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getChevrolet() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getAudi() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getEverything() + "</td>"
                    + "</tr>";
        }

        return stataTohtml;
    }

    public String stataToHtmlSummer(int count) {
        String stataTohtml = "<tr align =\"center\">"
                + "<td bgcolor = \"#F1F1F1\">" + "Go to the sea" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Study" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "Looking for a job" + "</td>"
                + "<td bgcolor = \"#F1F1F1\">" + "I choose everything in the summer" + "</td>"
                + "</tr>";
        for (int i = 1; i <= count; i++) {
            Anketa tmp = hashMap.get(i);
            stataTohtml = stataTohtml + "<tr align =\"center\">"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getSea() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getStudy() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getJob() + "</td>"
                    + "<td bgcolor = \"#F1F1F1\">" + stata.getEverythingSummer() + "</td>"
                    + "</tr>";
        }

        return stataTohtml;
    }

}
