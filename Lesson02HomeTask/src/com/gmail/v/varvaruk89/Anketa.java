package com.gmail.v.varvaruk89;

public class Anketa {
    private String fname = "";
    private String sname = "";
    private int age = 0;

    private String aCar = "everything";
    private String plansSummer = "everything";

    public Anketa(String fname, String sname, int age, String aCar, String plansSummer) {
        this.fname = fname;
        this.sname = sname;
        this.age = age;
        this.aCar = aCar;
        this.plansSummer = plansSummer;
    }

    public Anketa() {
    }

    public String getFname() {
        return fname;
    }

    public String getSname() {
        return sname;
    }

    public int getAge() {
        return age;
    }

    public String getaCar() {
        return aCar;
    }

    public String getPlansSummer() {
        return plansSummer;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setaCar(String aCar) {
        this.aCar = aCar;
    }

    public void setPlansSummer(String plansSummer) {
        this.plansSummer = plansSummer;
    }

    @Override
    public String toString() {
        return "Anketa{" +
                "fname='" + fname + '\'' +
                ", sname='" + sname + '\'' +
                ", age=" + age +
                ", aCar='" + aCar + '\'' +
                ", plansSummer='" + plansSummer + '\'' +
                '}';
    }



}
