package com.gmail.v.varvaruk89;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;


public class Connector {
    private static final String URL = "jdbc:mysql://localhost:3306/apartment";
    private static final String USERNAME = "test";
    private static final String PASSWORD = "test";


    public static void activationOfBd() {
        try {

            Connection connection = createConnection();
            createTableApartment(connection);
            connection.close();
        } catch (SQLException e) {
            System.out.println("Driver does not work!");

        }

    }


    private static void createTableApartment(Connection connection) {

        try {

            Statement st = connection.createStatement();

            // st.execute("DROP  TABLE IF EXISTS apartment  ");
            //   st.execute("CREATE  TABLE  apartment (id INT NOT NULL  AUTO_INCREMENT PRIMARY KEY,region TEXT DEFAULT NULL,address TEXT DEFAULT NULL,area DOUBLE  DEFAULT NULL,numberOfRooms INT DEFAULT  NULL,price DOUBLE DEFAULT NULL)");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static Statement createStatement() {
        Statement statement = null;

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return statement;
    }


    public static Connection createConnection() {
        Connection connection = null;

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
