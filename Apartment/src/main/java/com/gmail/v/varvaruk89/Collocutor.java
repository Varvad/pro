package com.gmail.v.varvaruk89;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Collocutor {

    private static Scanner sc = null;

    public static void communicate() {
        sc = new Scanner(System.in);
        System.out.println("To add an apartment press 1. To view apartment  press 2. And press Enter");
        int selection = 0;
        try {
            selection = sc.nextInt();
            if (selection == 1 | selection == 2) {
                if (selection == 1) {
                    addApartment();
                }
                if (selection == 2) {
                    outInformation();
                }
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("press 1 / press 2. And press Enter!");
        }

    }

    public static void outInformation() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("If you want to see all the information, press 1. If by parameter press 2");
        try {
            int selection = Integer.parseInt(reader.readLine());
            if (selection == 1 | selection == 2) {
                if (selection == 1) {
                    Editor.viewBD();
                }
                if (selection == 2) {
                    paramOut();
                }
            } else {
                throw new NumberFormatException();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public static void paramOut() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sql = "SELECT * FROM apartment WHERE ";
        System.out.println("To search by parameter region, press 1");
        System.out.println("To search by parameter address, press 2");
        System.out.println("To search by parameter area, press 3");
        System.out.println("To search by parameter numberOfRooms, press 4");
        System.out.println("To search by parameter price, press 5");
        try {
            String s  = reader.readLine();

            switch (s) {
                case "1":
                  System.out.println("Enter the value by which we will search");
                   sql = sql+"region = ?";
                   Editor.viewBDToParam(sql,reader.readLine(),1);
                    break;
                case "2":
                    System.out.println("Enter the value by which we will search");
                    sql = sql+"address = ?";
                    Editor.viewBDToParam(sql,reader.readLine(),1);
                    break;
                case "3":
                    System.out.println("Enter the value by which we will search");
                    sql = sql+"area = ?";
                    Editor.viewBDToParam(sql,reader.readLine(),3);
                    break;
                case "4":
                    System.out.println("Enter the value by which we will search");
                    sql = sql+"numberOfRooms = ?";
                    Editor.viewBDToParam(sql,reader.readLine(),2);
                    break;
                case "5":
                    System.out.println("Enter the value by which we will search");
                    sql = sql+"price = ?";
                    Editor.viewBDToParam(sql,reader.readLine(),3);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void addApartment() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String region;
        String address;
        double area;
        int numberOfRooms;
        double price;
        try {
            System.out.println("Enter the region:");
            region = reader.readLine();
            System.out.println("Enter the numberOfRooms:");
            numberOfRooms = Integer.parseInt(reader.readLine());
            System.out.println("Enter the area:");
            area = Double.parseDouble(reader.readLine());
            System.out.println("Enter the address:");
            address = reader.readLine();
            System.out.println("Enter the price:");
            price = Double.parseDouble(reader.readLine());
            Editor.addToBd(region, address, area, numberOfRooms, price);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
