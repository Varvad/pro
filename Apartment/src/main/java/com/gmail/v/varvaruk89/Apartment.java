package com.gmail.v.varvaruk89;


public class Apartment {
    private int id;
    private String region;
    private String address;
    private double area;
    private int numberOfRooms;
    private double price;

    public Apartment(int id, String region, String address, double area, int numberOfRooms, double price) {
        this.id = id;
        this.region = region;
        this.address = address;
        this.area = area;
        this.numberOfRooms = numberOfRooms;
        this.price = price;
    }

    public Apartment() {
    }

    public int getId() {
        return id;
    }

    public String getRegion() {
        return region;
    }

    public String getAddress() {
        return address;
    }

    public double getArea() {
        return area;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public double getPrice() {
        return price;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return
                "id=" + id +
                ", region='" + region + '\'' +
                ", address='" + address + '\'' +
                ", area=" + area +
                ", numberOfRooms=" + numberOfRooms +
                ", price=" + price ;
    }
}

