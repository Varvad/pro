package com.gmail.v.varvaruk89;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Editor {
    private static ResultSet rs;
    private static Statement statement;

    public static void viewBD() {
        statement = Connector.createStatement();
        try {
            rs = statement.executeQuery("SELECT  * FROM apartment");
            printBDConsol(rs);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void printBDConsol(ResultSet rs) {
        try {
            int i =0;
            while (rs.next()) {
                Apartment tmp = new Apartment();
                tmp.setId(rs.getInt("id"));
                tmp.setRegion(rs.getString("region"));
                tmp.setAddress(rs.getString("address"));
                tmp.setArea(rs.getDouble("area"));
                tmp.setNumberOfRooms(rs.getInt("numberOfRooms"));
                tmp.setPrice(rs.getDouble("price"));
                System.out.println(tmp);
                i++;
            }
            System.out.println("Found number of results = "+i);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void viewBDToParam(String sql, String param, int s) {
        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement(sql);
            if (s == 1) {
                preparedStatement.setString(1, param);
            }
            if (s == 2) {
                preparedStatement.setInt(1, Integer.parseInt(param));
            }
            if (s == 3) {
                preparedStatement.setDouble(1, Double.parseDouble(param));

            }
            rs = preparedStatement.executeQuery();
            printBDConsol(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addToBd(String region, String address, double area, int numberOfRooms, double price) {

        try {
            PreparedStatement preparedStatement = Connector.createConnection().prepareStatement("INSERT INTO apartment (region, address, area, numberOfRooms,price)\n" +
                    "   VALUES(?, ?, ?, ?,?)");
            preparedStatement.setString(1, region);
            preparedStatement.setString(2, address);
            preparedStatement.setDouble(3, area);
            preparedStatement.setInt(4, numberOfRooms);
            preparedStatement.setDouble(5, price);
            preparedStatement.executeUpdate();
            System.out.println("Add OK!");
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
