package com.gmail.v.varvaruk89;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@SaveTo(path = "D:\\fileTest.txt")
public class TextContainer {
    private String string = "Hello Vadum!";

    @Saver
    public void save(String path) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {
            bufferedWriter.write(string);
            System.out.println("Save to " + path);
        } catch (IOException e) {
            System.out.println(e);
        }
    }


}
