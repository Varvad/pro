package com.gmail.v.varvaruk89;


import java.lang.reflect.Method;

public class Main {
    public static void main(String[] arg) {
        TextContainer textContainer = new TextContainer();
        Class<?> cls = textContainer.getClass();


        try {
            Method[] methods = cls.getDeclaredMethods();
            SaveTo saveTo = cls.getAnnotation(SaveTo.class);
            for (Method m : methods) {
                if (m.isAnnotationPresent(Saver.class)) {
                    m.invoke(textContainer, saveTo.path());
                }
            }

        } catch (Exception e) {

        }

    }

}
