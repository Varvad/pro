package com.gmail.v.varvaruk89;

import java.io.*;

public class Filesmonster {

    public static void write(String str, File file) {
        try (PrintWriter pw = new PrintWriter(file)) {
            pw.write(str);
            // System.out.println("I wrote it in a file "+file.getAbsolutePath()+".");

        } catch (IOException r) {
            r.printStackTrace();
        }

    }

    public static String read(File file) {
        String result = "";
        try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
            String str = "";
            while ((str = bf.readLine()) != null) {
                result = str + result;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }

}
