package com.gmail.v.varvaruk89;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.InvalidParameterException;

public class Deserialize {
    public static <T> T deserialize(String s, Class<T> cls) throws IllegalAccessException, InstantiationException, NoSuchFieldException {
        T res = (T) cls.newInstance();
        //System.err.println("TEST"+s);
        String[] pairs = s.split(";");
        for (String p : pairs) {
            String[] nv = p.split("=");
            if (nv.length != 2) {
                throw new InvalidParameterException(s);
            }

            String name = nv[0];
            String value = nv[1];
            Field f = cls.getDeclaredField(name);
            if (Modifier.isPrivate(f.getModifiers())) {
                f.setAccessible(true);
            }
            if (f.isAnnotationPresent(Save.class)) {
                if (f.getType() == int.class) {
                    f.setInt(res, Integer.parseInt(value));
                } else if (f.getType() == String.class) {
                    f.set(res, value);
                }
            }
        }
        return res;

    }

}
