package com.gmail.v.varvaruk89;


import java.io.File;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, NoSuchFieldException, InstantiationException {
        Car bmw = new Car("BMW", 1999, "blue");
        String objstr = "";
        File file = new File("test.txt");
        System.out.println(bmw);
        try {
            objstr = Serializer.serializeString(bmw);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Filesmonster.write(objstr, file);
        System.out.println();


        Car desrBMW = Deserialize.deserialize(Filesmonster.read(file), Car.class);
        System.out.println("Magic " + desrBMW);


    }
}
