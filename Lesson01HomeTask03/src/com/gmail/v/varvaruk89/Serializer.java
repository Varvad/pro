package com.gmail.v.varvaruk89;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Serializer {

    public static String serializeString(Object o) throws IllegalAccessException {
        Class<?> cls = o.getClass();
        StringBuilder sb = new StringBuilder();

        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            if (!f.isAnnotationPresent(Save.class)) {
                continue;
            }
            if (Modifier.isPrivate(f.getModifiers())) {
                f.setAccessible(true);
            }
            sb.append(f.getName() + "=");

            if (f.getType() == int.class) {
                sb.append(f.getInt(o));
            } else if (f.getType() == String.class) {
                sb.append((String) f.get(o));
            }
            sb.append(";");
        }
        return sb.toString();
    }


}
