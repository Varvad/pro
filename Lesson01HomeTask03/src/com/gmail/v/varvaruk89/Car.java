package com.gmail.v.varvaruk89;

public class Car {
    public String name = "";
    @Save
    public int years;
    @Save
    public String color;
    @Save
    private int price;


    public Car(String name, int years, String color) {
        this.name = name;
        this.years = years;
        this.color = color;
        this.price = years * 10;
    }

    public Car() {
    }


    public int getYears() {

        return years;
    }

    public String getColor() {

        return color;
    }

    public void setYears(int years) {

        this.years = years;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "years=" + years +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}
