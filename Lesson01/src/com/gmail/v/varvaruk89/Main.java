package com.gmail.v.varvaruk89;

import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {

        final Class<?> testClass = MyClass.class;
        MyClass myClass = new MyClass();
        try {
            Method[] method = testClass.getMethods();
            for (Method m : method) {

                if (m.isAnnotationPresent(Test.class)) {

                    Test test = m.getAnnotation(Test.class);
                    m.invoke(myClass, test.a(), test.b());
                }
            }
        } catch (Exception e) {
            System.out.print(e);
        }

    }
}
